const char PROGMEM sinetable[128] = {
  0, 0, 0, 0, 1, 1, 1, 2, 2, 3, 4, 5, 5, 6, 7, 9, 10, 11, 12, 14, 15, 17, 18, 20, 21, 23, 25, 27, 29, 31, 33, 35, 37, 40, 42, 44, 47, 49, 52, 54, 57, 59, 62, 65, 67, 70, 73, 76, 79, 82, 85, 88, 90, 93, 97, 100, 103, 106, 109, 112, 115, 118, 121, 124,
  128, 131, 134, 137, 140, 143, 146, 149, 152, 155, 158, 162, 165, 167, 170, 173, 176, 179, 182, 185, 188, 190, 193, 196, 198, 201, 203, 206, 208, 211, 213, 215, 218, 220, 222, 224, 226, 228, 230, 232, 234, 235, 237, 238, 240, 241, 243, 244, 245, 246, 248, 249, 250, 250, 251, 252, 253, 253, 254, 254, 254, 255, 255, 255,
};
unsigned int   frequency2, _phase2, frequency3, _phase3, timbre, envelopeDecay, envelopeAmplitude, mod;

#define NUMBER_OF_OSCILLATORS 3
unsigned int _phase[NUMBER_OF_OSCILLATORS];
unsigned int frequency[NUMBER_OF_OSCILLATORS];
unsigned int amplitude[NUMBER_OF_OSCILLATORS];
unsigned int mainAmplitude;
int detune;
bool envelopeGate;
uint8_t waveform;
uint8_t octave;

//unsigned int quantizedPitch[12]={262,277,294,311,330, 349,370,392,415,440,466,494};

unsigned int quantizedPitch[12]={2093,2217,2349,2489, 2637,2794,2960,3136,3322,3520,3729,3951};


void setWave(uint8_t _wave){
  waveform=_wave;
}
void initAudio() {
  waveform=0;
  sineWave();
  setFrequency(400, 0);
  setFrequency(400, 1);
  setFrequency(400, 2);

  setAmplitude(400, 0);
  setAmplitude(400, 1);
  setAmplitude(400, 2);
  //setTimbre(100);
}
void setDetune(int _detune) {
  detune = _detune;
}
void setMainAmplitude(uint16_t _amplitude) {
  mainAmplitude = _amplitude >> 2;
}
void setOctave(int _oct){
  octave=_oct>>8;
}
void setSemitone(uint16_t _semitone){
  
  int _oct=8-(_semitone/12);
  uint8_t _semi=_semitone%12;
  setMainFrequency(quantizedPitch[_semitone]>>_oct);
}
void setMainFrequency(uint16_t _freq) {
  
  frequency[0] = (_freq << octave) - 13;
  if(waveform==1 || waveform==2) frequency[1] = frequency[0]>>2;
  else frequency[1] = frequency[0]>>1;
  frequency[2] = frequency[0]+detune;
  
 // setFrequency(_freq, 0);
  //setFrequency(_freq, 1);
  //if(waveform==1 || waveform==2) setFrequency(_freq / 4, 1);
  //else setFrequency(_freq / 2, 1);
 // setFrequency(_freq + detune, 2);
}
void setFrequency(uint16_t _freq, uint8_t _oscillator) {
  frequency[_oscillator] = (_freq << 1) - 13;
  //frequency2 = frequency ;
}
void setAmplitude(uint16_t _amplitude, uint8_t _oscillator) {
  amplitude[_oscillator] = ((_amplitude >> 2) * mainAmplitude) >> 8;
}

unsigned char wavetable[256];
void sineWave() {                                       //too costly to calculate on the fly, so it reads from the sine table. We use 128 values, then mirror them to get the whole cycle
  for (int i = 0; i < 128; ++i) {
    wavetable[i] = pgm_read_byte_near(sinetable + i);
  }
  wavetable[128] = 255;
  for (int i = 129; i < 256; ++i) {
    wavetable[i] = wavetable[256 - i] ;
  }
}

void sawtoothWave() {
  for (int i = 0; i < 256; ++i) {
    wavetable[i] = i; // sawtooth
  }
}
void triangleWave() {
  for (int i = 0; i < 128; ++i) {
    wavetable[i] = i * 2;
  }
  int value = 255;
  for (int i = 128; i < 256; ++i) {
    wavetable[i] = value;
    value -= 2;
  }
}
void squareWave() {
  for (int i = 0; i < 128; ++i) {
    wavetable[i] = 255;
  }
  for (int i = 128; i < 256; ++i) {
    wavetable[i] = 1;                  //0 gives problems (offset and different freq), related to sample  = ((wavetable[phase >> 8]*amplitude)>>8);
  }
}
void zeroWave() {
  for (int i = 0; i < 256; ++i) {
    wavetable[i] = 1;                  //0 gives problems
  }
}
void setTimbre(uint16_t _timbre) {
  timbre = _timbre;
}
void setDecay(uint16_t _decay) {
  envelopeDecay =  map(_decay, 0, 1023, 300, 7) ;

}

void setEnvelopeGate(bool _gate) {
  envelopeGate = _gate;
}

void renderEnvelope() {

  if (envelopeGate == true) {
    envelopeAmplitude = 16383;
  }
  else {
    if (envelopeAmplitude > envelopeDecay) {
      envelopeAmplitude -= envelopeDecay;
    }
    else if (envelopeAmplitude > 0)  envelopeAmplitude -= 1;
  }
}

void setMod(unsigned int _mod) {
  mod = _mod >> 2;
}



void updateAudioEngine() { //rendered at 33khz
  // incr+=8;




  // _phase2 += frequency2;
  //uint16_t _phs = (_phase + (wavetable[_phase2 >> 8] * (timbre + (((envelopeAmplitude >> 6) * mod) >> 6))));
  //uint16_t output = (wavetable[_phs >> 8] * (amplitude + (envelopeAmplitude >> 6))) >> 6;

  for (uint8_t i = 0; i < 3; i++) {
    /*
     if(i==1 && !(_phase[1] >> 15)) _phase[1]=_phase[0]>>1;
     else{
      
     }
*/
     _phase[i] +=frequency[i];
    //if(i==1) _phase[i] += (frequency[0]>>1);
  }
 
  uint16_t output = 0;
  
  switch(waveform){
    case 0:
    output = ((wavetable[_phase[0] >> 8] * amplitude[0]) >> 8) + ((_phase[1] >> 15) * amplitude[1]) + (((_phase[2] >> 8) * amplitude[2]) >> 8) ;
    break;
    case 1:
    output = ((_phase[0] >> 15) * amplitude[0]) + ((_phase[1] >> 15) * amplitude[1]) + ((_phase[2] >> 15) * amplitude[2]) ;
    break;
    case 2:
    output = (((_phase[0] >> 8) * amplitude[0]) >> 8) + ((_phase[1] >> 15) * amplitude[1]) + (((_phase[0] >> 8) * amplitude[0]) >> 8) + ((wavetable[_phase[2] >> 8] * amplitude[2]) >> 8) ;
    break;
    default:
    output = ((wavetable[_phase[0] >> 8] * amplitude[0]) >> 8) + ((wavetable[_phase[1] >> 8] * amplitude[1]) >> 8) + ((wavetable[_phase[2] >> 8] * amplitude[2]) >> 8) ;
    break;
  }
  if (assignedCV2 == ASSIGNED_TO_SYNTH) OCR1AH = highByte(output), OCR1AL = lowByte(output); //Timer1.pwm(9, output);

}
