



#define AVG_DEPTH 16
int circularBuffer[8][AVG_DEPTH]; //256 bytes in the memory (total memory is 2056 bytes)
uint8_t circularIndex = 0;
int envelopeFollower[8];
#define DECAY_RATE 4 // 4 ~ 1s , 80 ~ 20hz cutoff

void renderFinalValues() {

  uint8_t i =  hw.getReadIndex(); //we dont need to calculate everything all the time because only one channel is updated in each run of the loop function

  lastFinalValue[i] = finalValue[i];
  if (i == 5) finalValue[i] = hw.getDigitalSensorValue(); //digitalSensor readout is not analog value
  else finalValue[i] = hw.getAnalogValue(i); //get value from the hw

  finalValue[i] = map(finalValue[i], channelMin[i], channelMax[i], 0, 1023); //map it based on automatic calibration
  finalValue[i] = constrain(finalValue[i], 0, 1023); //make sure it clips and does not overflow
  if (invertState[i])  finalValue[i] = 1023 - finalValue[i]; //invert the value if inversion is ON

  //average
  if (i == 0) circularIndex++;
  if (circularIndex >= AVG_DEPTH) circularIndex = 0;
  circularBuffer[i][circularIndex] = finalValue[i];
  uint16_t average = 0;
  for (uint8_t j = 0; j < AVG_DEPTH; j++) {
    average = average + circularBuffer[i][j];
  }

  average = average >> 4; //optimized version for divided by 16 - simple bitshift ... is is the same as ~ average = average / AVG_DEPTH;
  //00001011 >>1 = 00000101 = /2
  //00000101 >>1 = 00000010 = /2
  if (bitRead(extractionSettings[i], AVERAGING)) {
    finalValue[i] = average;
  }

  //envelope follower rendering
  if (envelopeFollower[i] > DECAY_RATE) envelopeFollower[i] = envelopeFollower[i] - DECAY_RATE;
  if (finalValue[i] > envelopeFollower[i]) envelopeFollower[i] = finalValue[i];
  if (bitRead(extractionSettings[i], ENVELOPE_FOLLOWER)) {
    finalValue[i] = envelopeFollower[i];
  }

  //SET GATE ACCORDING TO THRESHOLD
  lastGateState[i] = gateState[i];
  if (bitRead(extractionSettings[i], THRESHOLD_INVERSION)) {
    if (inBetween(finalValue[i], channelThreshold[i], channelThreshold2[i])) gateState[i] = false;
    else gateState[i] = true;
  }
  else {
    if (inBetween(finalValue[i], channelThreshold[i], channelThreshold2[i])) gateState[i] = true;
    else gateState[i] = false;
  }

}
